/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "rc522.h"
#include <stdio.h>
#include <string.h>
#include "MFRC522c.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/**
 * Helper routine to dump a uint8_t array as hex values to Serial.
 */
void dump_byte_array(uint8_t *buffer, uint8_t bufferSize) {
    for (uint8_t i = 0; i < bufferSize; i++) {
        PRINT("0x%02x ", buffer[i]);
    }
}



//
//MFRC522 mfrc522;   // Create MFRC522 instance.


char buff[64] = {0};

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI2_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  
  LL_SPI_Enable(RFID_SPI);
  LL_GPIO_ResetOutputPin(RFID_RST_GPIO_Port, RFID_RST_Pin);
  HAL_Delay(10);
  LL_GPIO_SetOutputPin(RFID_RST_GPIO_Port, RFID_RST_Pin);
  
  MFRC522_PCD_Init(); // Init MFRC522 card
  
  // Prepare the key (used both as key A and as key B)
  // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
  for (uint8_t i = 0; i < 6; i++) {
      key.keyByte[i] = 0xFF;
  }
  
  PRINT("Scan a MIFARE Classic PICC to demonstrate read and write.\r\n");
  PRINT("Using key (for A and B):");
  dump_byte_array(key.keyByte, MF_KEY_SIZE);
  PRINT("\r\nBEWARE: Data will be written to the PICC, in sector #1\r\n");
  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  
    HAL_Delay(1000);
     // Look for new cards
    if ( ! MFRC522_PICC_IsNewCardPresent())
        continue;

    // Select one of the cards
    if ( ! MFRC522_PICC_ReadCardSerial())
        continue;

    // Show some details of the PICC (that is: the tag/card)
    PRINT("Card UID:");
    dump_byte_array(uid.uidByte, uid.size);
    PRINT("\r\nPICC type: ");
    MFRC522_PICC_Type piccType = MFRC522_PICC_GetType(uid.sak);
    PRINT("%s\r\n", MFRC522_PICC_GetTypeName(piccType));

    // Check for compatibility
    if (    piccType != PICC_TYPE_MIFARE_MINI
        &&  piccType != PICC_TYPE_MIFARE_1K
        &&  piccType != PICC_TYPE_MIFARE_4K) {
        PRINT("This sample only works with MIFARE Classic cards.\r\n");
        continue;
    }

    // In this sample we use the second sector,
    // that is: sector #1, covering block #4 up to and including block #7
    uint8_t sector         = 1;
    uint8_t blockAddr      = 4;
    uint8_t dataBlock[]    = {
        0x04, 0x08, 0x15, 0x16, //  1,  2,   3,  4,
        0x23, 0x42, 0x00, 0x11, //  5,  6,   7,  8,
        0x22, 0x33, 0x44, 0x55, //  9, 10, 255, 12,
        0x66, 0x77, 0x88, 0x99  // 13, 14,  15, 16
    };
    uint8_t trailerBlock   = 7;
    MFRC522_StatusCode status;
    uint8_t buffer[18];
    uint8_t size = sizeof(buffer);

    // Authenticate using key A
    PRINT("Authenticating using key A...\r\n");
    status = (MFRC522_StatusCode) MFRC522_PCD_Authenticate(PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &uid);
    if (status != STATUS_OK) {
        PRINT("PCD_Authenticate() failed: %s\r\n", MFRC522_GetStatusCodeName(status));
        continue;
    }

    // Show the whole sector as it currently is
    PRINT("Current data in sector:\r\n");
    MFRC522_PICC_DumpMifareClassicSectorToSerial(&uid, &key, sector);
    PRINT("\r\n");

    // Read data from the block
    PRINT("Reading data from block %d ...\r\n", blockAddr);
    status = (MFRC522_StatusCode) MFRC522_MIFARE_Read(blockAddr, buffer, &size);
    if (status != STATUS_OK) {
        PRINT("MIFARE_Read() failed: ");
        PRINT("%s\r\n", MFRC522_GetStatusCodeName(status));
    }
    PRINT("Data in block %d:\r\n", blockAddr);
    dump_byte_array(buffer, 16);
    PRINT("\r\n\r\n");

    // Authenticate using key B
    PRINT("Authenticating again using key B...\r\n");
    status = (MFRC522_StatusCode) MFRC522_PCD_Authenticate(PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &uid);
    if (status != STATUS_OK) {
        PRINT("PCD_Authenticate() failed: %s\r\n", MFRC522_GetStatusCodeName(status));
        continue;
    }

    // Write data to the block
    PRINT("Writing data into block %d ...\r\n", blockAddr);
    dump_byte_array(dataBlock, 16); PRINT("");
    status = (MFRC522_StatusCode) MFRC522_MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != STATUS_OK) {
        PRINT("\r\nMIFARE_Write() failed: %s\r\n", MFRC522_GetStatusCodeName(status));
    }
    PRINT("\r\n\r\n");

    // Read data from the block (again, should now be what we have written)
    PRINT("Reading data from block %d ...", blockAddr);
    status = (MFRC522_StatusCode) MFRC522_MIFARE_Read(blockAddr, buffer, &size);
    if (status != STATUS_OK) {
        PRINT("\r\nMIFARE_Read() failed: ");
        PRINT("%s\r\n", (MFRC522_GetStatusCodeName(status)));
    }
    PRINT("\r\nData in block %d:\r\n", blockAddr);
    dump_byte_array(buffer, 16); 
    
    // Check that data in block is what we have written
    // by counting the number of bytes that are equal
    PRINT("\r\nChecking result...\r\n");
    uint8_t count = 0;
    for (uint8_t i = 0; i < 16; i++) {
        // Compare buffer (= what we've read) with dataBlock (= what we've written)
        if (buffer[i] == dataBlock[i])
            count++;
    }
    PRINT("Number of bytes that match = %d\r\n", count);
    if (count == 16) {
        PRINT("Success :-)\r\n");
    } else {
        PRINT("Failure, no match :-(\r\n  perhaps the write didn't work properly...\r\n");
    }

    // Dump the sector data
    PRINT("\r\nCurrent data in sector:");
    MFRC522_PICC_DumpMifareClassicSectorToSerial(&uid, &key, sector);
    PRINT("\r\n");

    // Halt PICC
    MFRC522_PICC_HaltA();
    // Stop encryption on PCD
    MFRC522_PCD_StopCrypto1();
//    
    
    
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
